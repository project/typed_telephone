<?php

namespace Drupal\typed_telephone\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\telephone\Plugin\Field\FieldType\TelephoneItem;

/**
 * Plugin implementation of the 'typed_telephone' field type.
 *
 * @FieldType(
 *   id = "typed_telephone",
 *   label = @Translation("Typed telephone"),
 *   description = @Translation("Typed telephone"),
 *   default_widget = "typed_telephone_default",
 *   default_formatter = "typed_telephone_default"
 * )
 */
class TypedTelephoneItem extends TelephoneItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['teltype'] = [
      'type' => 'varchar',
      'length' => 30,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['teltype'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Telephone Type'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', [
      'teltype' => [
        'Length' => [
          'max' => 30,
          'maxMessage' => $this->t('%name: type may not be longer than 30 characters.', [
            '%name' => $this->getFieldDefinition()->getLabel(),
          ]),
        ],
      ],
    ]);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = parent::generateSampleValue($field_definition);
    $values['teltype'] = array_rand($field_definition->getSetting('allowed_types'), 1);
    return $values;
  }


  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'allowed_types' => ''
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];
    // Is it even possible to inject ConfigManager here?
    $config_helper = \Drupal::service('typed_telephone.confighelper');

    $elements['allowed_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed telephone types'),
      '#default_value' => $this->getSetting('allowed_types')??$config_helper->getTypesAsOptions(),
      '#required' => TRUE,
      '#description' => $this->t('The allowed telephone types for this field.'),
      '#options' => $config_helper->getTypesAsOptions(),
      '#disabled' => $has_data,
    ];

    return $elements;
  }

}
