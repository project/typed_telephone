<?php

namespace Drupal\typed_telephone\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telephone\Plugin\Field\FieldWidget\TelephoneDefaultWidget;

/**
 * Plugin implementation of the 'typed_telephone_default' widget.
 *
 * @FieldWidget(
 *   id = "typed_telephone_default",
 *   module = "typed_telephone",
 *   label = @Translation("Typed telephone"),
 *   field_types = {
 *     "typed_telephone"
 *   }
 * )
 */
class TypedTelephoneDefaultWidget extends TelephoneDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $tel_element = parent::formElement($items, $delta, $element, $form, $form_state);

    \Drupal::logger('ttel')->notice('Element: <pre>'.print_r(count($items),1).'</pre>');
    $element['details'] = [
      '#type' => 'fieldset',
      '#title' => $element['#title'],
      '#open' => TRUE,
      '#description' => $element['#description'],
    ];

    // To avoid repeating the whole group on multivalue fields, replace its
    // definition without changing structure.
    // @todo: find a better way to detect cardinality here.
    if(is_object($element['#title'])) {
      $element['details'] = [
        '#type' => 'html_tag',
        '#tag' => 'div'
      ];
    }

    $config_helper = \Drupal::service('typed_telephone.confighelper');

    $element['details']['teltype'] = [
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#options' => $config_helper->getTypesAsOptions($this->getFieldSetting('allowed_types')),
      '#required' => $element['#required'],
      '#prefix' => '<div class="container-inline">'
    ];

    $element['details']['value'] = $tel_element['value'];
    $element['details']['value']['#suffix'] = '</div>';

    // Remove some visually interfering elements from default tel element.
    unset($element['details']['value']['#title']);
    unset($element['details']['value']['#description']);


    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['teltype'] = $value['details']['teltype'];
      $value['value'] = $value['details']['value'];
      unset($value['details']);
    }

    return $values;
  }

}
